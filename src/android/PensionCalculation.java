package cordova-plugin-pensioncalculation;

import org.apache.cordova.CordovaPlugin;
import org.apache.cordova.CallbackContext;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * This class echoes a string called from JavaScript.
 */
public class PensionCalculation extends CordovaPlugin {

    @Override
    public boolean execute(String action, JSONArray args, CallbackContext callbackContext) throws JSONException {

         if (action.equals("calculate")) {
            this.calculate(args, callbackContext);
            return true;
        }
        return false;
    }

    private void calculate(JSONArray data, CallbackContext callbackContext){
        if(data!=null){
            try{
                int averageSalary=Integer.parseInt(data.getJSONObject(0).getString("param1"));
                int workYears=Integer.parseInt(data.getJSONObject(0).getString("param2"));
                int result=averageSalary*workYears*1.4;
            }
            catch(Exception ex){
                callbackContext.error("Error : "+ex);
            }
        }
        else{
            callbackContext.error("No value passed");
        }
    }
}
