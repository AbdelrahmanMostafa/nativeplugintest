/********* PensionCalculation.m Cordova Plugin Implementation *******/

#import <Cordova/CDV.h>

@interface PensionCalculation : CDVPlugin {
  // Member variables go here.
}

- (void)calculate:(CDVInvokedUrlCommand*)command;
@end

@implementation PensionCalculation
- (void)calculate:(CDVInvokedUrlCommand*)command
{
    CDVPluginResult* pluginResult = nil;
    NSString* averageSalary = [[command.arguments objectAtIndex:0] valueForKey:@"param1"];
    NSString* workYears = [[command.arguments objectAtIndex:0] valueForKey:@"param2"];
    if(averageSalary>=0&&workYears>=0){
    NSString* result = @(averageSalary*1.4*workYears);
        pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:result];

    }
    else{
        pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR];
    }
    [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];

}

@end
