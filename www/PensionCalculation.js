var exec = require('cordova/exec');

module.exports.calculate = function (arg0, success, error) {
    exec(success, error, 'PensionCalculation', 'calculate', [arg0]);
}